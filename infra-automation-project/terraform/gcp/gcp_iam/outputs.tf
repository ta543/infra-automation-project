output "service_account_email" {
  value       = google_service_account.default.email
  description = "The email of the created service account"
}

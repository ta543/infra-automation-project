output "application_id" {
  value = azuread_application.example.application_id
}

output "service_principal_id" {
  value = azuread_service_principal.example.object_id
}

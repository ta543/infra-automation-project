output "vpc_id" {
  value = aws_vpc.main.id
  description = "The ID of the VPC."
}

output "subnet_ids" {
  value = aws_subnet.main[*].id
  description = "The IDs of the subnets created in the VPC."
}

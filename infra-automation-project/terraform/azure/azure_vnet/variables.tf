variable "vnet_name" {
  description = "The name of the virtual network."
  type        = string
  default     = "myVnet"
}

variable "address_space" {
  description = "The address space that is used by the virtual network."
  type        = string
  default     = "10.0.0.0/16"
}

variable "location" {
  description = "The location/region where the virtual network is created."
  type        = string
  default     = "East US"
}

variable "resource_group_name" {
  description = "The name of the resource group in which to create the virtual network."
  type        = string
  default     = "myResourceGroup"
}

variable "subnet_name" {
  description = "The name of the subnet."
  type        = string
  default     = "mySubnet"
}

variable "subnet_prefix" {
  description = "The address prefix to use for the subnet."
  type        = string
  default     = "10.0.1.0/24"
}

variable "tags" {
  description = "A mapping of tags to assign to the resource."
  type        = map(string)
  default     = {
    Environment = "Development"
  }
}

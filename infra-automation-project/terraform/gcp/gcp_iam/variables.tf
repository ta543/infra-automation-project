variable "project_id" {
  description = "The GCP project ID"
  type        = string
}

variable "service_account_id" {
  description = "The ID of the service account to create"
  type        = string
  default     = "my-service-account"
}

variable "custom_role" {
  description = "A custom role to assign to the service account"
  type        = string
  default     = "roles/logging.logWriter"
}

variable "project" {
  description = "The GCP project ID."
  type        = string
  default     = "87327382"
}

output "aws_region" {
  value = var.region
  description = "The AWS region used for resource deployment"
}

output "gcp_region" {
  value = var.gcp_region
  description = "The GCP region used for resource deployment"
}

output "azure_region" {
  value = var.azure_region
  description = "The Azure region used for resource deployment"
}


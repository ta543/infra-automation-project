output "eks_cluster_iam_role_arn" {
  value = aws_iam_role.eks_cluster.arn
}

output "eks_cluster_role_arn" {
  value       = aws_iam_role.eks_cluster.arn
  description = "The ARN of the IAM role used by the EKS cluster."
}

output "node_role_arn" {
  value       = aws_iam_role.node.arn
  description = "The ARN of the IAM role used by EKS nodes."
}

output "node_policy_attachment_id" {
  value = aws_iam_role_policy_attachment.node_AmazonEKSWorkerNodePolicy.id
  description = "ID of the policy attachment for the EKS node IAM role."
}

output "node_policy_arn" {
  value       = aws_iam_role_policy_attachment.node_AmazonEKSWorkerNodePolicy.policy_arn
  description = "The ARN of the policy attached to the EKS node role."
}

output "node_policy_attachment_arn" {
  value = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  description = "ARN of the policy attached to the EKS node IAM role."
}

output "iam_role_arn" {
  value = aws_iam_role.eks_role.arn
  description = "The ARN of the IAM role used by EKS."
}





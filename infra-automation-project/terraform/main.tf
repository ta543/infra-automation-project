module "aws_vpc" {
  source = "./aws/aws_vpc"
}

module "aws_eks" {
  source = "./aws/aws_eks"
  eks_cluster_iam_role_arn   = module.aws_iam.eks_cluster_iam_role_arn
  node_role_arn              = module.aws_iam.node_role_arn
  node_policy_attachment_arn = module.aws_iam.node_policy_attachment_arn
  node_policy_attachment_id  = module.aws_iam.node_policy_attachment_id
  iam_role_arn               = module.aws_iam.iam_role_arn
}

module "aws_iam" {
  source = "./aws/aws_iam"
}

module "gcp_vpc" {
  source = "./gcp/gcp_vpc"
}

module "gcp_gke" {
  source = "./gcp/gcp_gke"
}

module "gcp_iam" {
  source           = "./gcp/gcp_iam"
  project_id = var.project
}

module "azure_vnet" {
  source              = "./azure/azure_vnet"
}

module "azure_aks" {
  source              = "./azure/azure_aks"
}

module "azure_iam" {
  source              = "./azure/azure_iam"
}

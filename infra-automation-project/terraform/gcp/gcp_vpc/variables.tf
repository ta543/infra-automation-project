variable "network_name" {
  description = "The name of the VPC network"
  type        = string
  default     = "my-vpc-network"
}

variable "subnetwork_name" {
  description = "The name of the subnet"
  type        = string
  default     = "my-subnetwork"
}

variable "subnetwork_cidr" {
  description = "The CIDR block for the subnet"
  type        = string
  default     = "10.0.1.0/24"
}

variable "region" {
  description = "The region where the subnet will be created"
  type        = string
  default     = "us-central1"
}

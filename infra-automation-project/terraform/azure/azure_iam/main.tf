resource "azuread_application" "example" {
  display_name = "example-application"
}

resource "azuread_service_principal" "example" {
  application_id = azuread_application.example.application_id
}

resource "azurerm_role_assignment" "example" {
  scope                = "/subscriptions/your_subscription_id"
  role_definition_name = "Contributor"
  principal_id         = azuread_service_principal.example.object_id
}

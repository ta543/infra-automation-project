variable "cluster_name" {
  description = "The name of the EKS cluster"
  type        = string
  default     = "my-eks-cluster"
}

variable "subnet_ids" {
  description = "A list of subnet IDs to launch the cluster in"
  type        = list(string)
  default     = ["subnet-12345678", "subnet-87654321"]
}

variable "node_desired_size" {
  description = "Desired number of worker nodes"
  type        = number
  default     = 2
}

variable "node_max_size" {
  description = "Maximum number of worker nodes"
  type        = number
  default     = 3
}

variable "node_min_size" {
  description = "Minimum number of worker nodes"
  type        = number
  default     = 1
}

variable "eks_cluster_iam_role_arn" {
  description = "The ARN of the IAM role for the EKS cluster"
  type        = string
}

variable "iam_role_arn" {
  description = "The ARN of the IAM role for the EKS nodes"
  type        = string
}

variable "node_role_arn" {
  description = "ARN of the IAM role for EKS nodes"
  type        = string
}

variable "node_policy_attachment_id" {
  description = "ID of the IAM policy attachment for EKS nodes"
  type        = string
}

variable "node_policy_attachment_arn" {
  description = "ARN of the IAM policy attachment for EKS nodes"
  type        = string
}

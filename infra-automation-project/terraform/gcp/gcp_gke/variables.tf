variable "cluster_name" {
  description = "The name of the GKE cluster"
  type        = string
  default     = "my-gke-cluster"
}

variable "location" {
  description = "The location (region or zone) of the GKE cluster"
  type        = string
  default     = "us-central1"
}

variable "node_count" {
  description = "The number of nodes to create in each of the cluster's zones"
  type        = number
  default     = 1
}

variable "preemptible" {
  description = "Determines if the nodes are preemptible"
  type        = bool
  default     = false
}

variable "machine_type" {
  description = "The machine type of the GKE nodes"
  type        = string
  default     = "e2-medium"
}

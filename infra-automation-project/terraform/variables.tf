variable "region" {
  description = "The AWS region for resource deployment"
  type        = string
  default     = "us-east-1"
}

variable "gcp_region" {
  description = "The GCP region for resource deployment"
  type        = string
  default     = "us-central1"
}

variable "azure_region" {
  description = "The Azure region for resource deployment"
  type        = string
  default     = "eastus"
}

variable "project" {
  description = "The GCP project ID."
  type        = string
  default     = "87327382"
}

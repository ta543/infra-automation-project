output "cluster_name" {
  value       = google_container_cluster.gke_cluster.name
  description = "The name of the GKE cluster"
}

output "cluster_endpoint" {
  value       = google_container_cluster.gke_cluster.endpoint
  description = "The cluster endpoint of the GKE cluster"
}

output "cluster_ca_certificate" {
  value       = google_container_cluster.gke_cluster.master_auth[0].cluster_ca_certificate
  description = "The cluster ca certificate (base64 encoded)"
}

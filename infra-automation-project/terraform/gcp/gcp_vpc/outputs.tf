output "network_name" {
  value       = google_compute_network.vpc_network.name
  description = "The name of the VPC network"
}

output "subnetwork_name" {
  value       = google_compute_subnetwork.subnetwork.name
  description = "The name of the subnet"
}

output "subnetwork_cidr" {
  value       = google_compute_subnetwork.subnetwork.ip_cidr_range
  description = "The CIDR block of the subnet"
}

resource "google_service_account" "default" {
  account_id   = var.service_account_id
  display_name = "Service Account for ${var.project_id}"
}

resource "google_project_iam_member" "project" {
  project = var.project
  role    = "roles/editor"
  member  = "serviceAccount:${google_service_account.default.email}"
}

resource "google_project_iam_member" "custom_role" {
  project = var.project
  role    = var.custom_role
  member  = "serviceAccount:${google_service_account.default.email}"
}

variable "application_name" {
  description = "The name of the Azure AD application."
  type        = string
  default     = "example-application"
}

variable "scope" {
  description = "The scope at which the role assignment applies to, e.g., the whole subscription or a specific resource group."
  type        = string
  default     = "/subscriptions/YOUR_SUBSCRIPTION_ID" # Replace YOUR_SUBSCRIPTION_ID with your actual subscription ID
}

variable "role_definition_name" {
  description = "The name of the role definition to assign."
  type        = string
  default     = "Contributor"
}
